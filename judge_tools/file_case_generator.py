# -*- coding: utf-8 -*-
import os
import shutil

# List of generators to run when the generate_cases function is called,
# and counter used to number the input files.
_generators = []
_case_counter = 1

# Class used to store a file generator to append to the output file.
class _TestCaseFile:
  # Initialize the generator with the name of the file to append.
  def __init__(self, *, input_file):
    self.input_file = input_file

  def _copy_file(self, stem, dir, file_path):
    # Generate the filename for the input file and increase the counter.
    # Then, copy the file to the output directory.
    global _case_counter
    number, _case_counter = _case_counter, _case_counter + 1
    output_filename = "{stem}.in.{n}".format(stem = stem, n = number)
    output_path = os.path.normpath(os.path.join(dir, output_filename))
    shutil.copyfile(file_path, output_path)

  # Run the generator and write all test cases to the specified output file.
  def run(self, stem, dir):
    # Check if the filename points to a file or a directory.
    if os.path.isfile(self.input_file):
      # If a file was specified, copy it to the output directory.
      self._copy_file(stem, dir, self.input_file)
    elif os.path.isdir(self.input_file):
      # If a directory was specified, copy each file into it to the output directory.
      for dirpath, dirnames, filenames in os.walk(self.input_file):
        for filename in filenames:
          # Generate the complete path to the file and copy it.
          file_path = os.path.normpath(os.path.join(dirpath, filename))
          self._copy_file(stem, dir, file_path)

# Class used to store a function to generate test cases.
class _TestCaseFunction:
  # Initialize the generator with the specified function and count.
  def __init__(self, *, function, count):
    self.function = function
    self.count = count

  # Run the generator and write all test cases to the specified output file.
  def run(self, stem, dir):
    # Call the generator function as many times at it was specified, with a
    # different output file each time and giving the index to each call.
    for index in range(self.count):
      # Generate the filename for the input file and increase the counter.
      global _case_counter
      number, _case_counter = _case_counter, _case_counter + 1
      output_filename = "{stem}.in.{n}".format(stem = stem, n = number)
      output_path = os.path.normpath(os.path.join(dir, output_filename))

      # Open the output file and make the function write in it.
      output = open(output_path, "wt")
      self.function(index, output)
      output.close()

# Add a file to the list of case files to append.
def add_file(*, file):
  _generators.append(_TestCaseFile(input_file = file))

# Add a function generator to the list of generators.
def add_function(*, f, n = 1):
  _generators.append(_TestCaseFunction(function = f, count = n))

# Generate all cases and write them to the specified output file.
def generate_cases(*, stem, dir):
  # Cleanse the specified directory and recreate it empty.
  if os.path.exists(dir):
    shutil.rmtree(dir)
  os.makedirs(dir)

  # Write the case count at the begining of the file, the run all generators
  # to the specified output file.
  global _case_counter
  _case_counter = 1
  for generator in _generators: generator.run(stem, dir)
