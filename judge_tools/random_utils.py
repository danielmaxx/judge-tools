# -*- coding: utf-8 -*-
import heapq

# Import basic functions from the random module.
from random import choice, random, sample, seed, shuffle, uniform
import random

# Common characters groups used to generate subsets.
DIGITS = "".join(chr(c) for c in range(ord('0'), ord('9') + 1))
UPPERCASE = "".join(chr(c) for c in range(ord('A'), ord('Z') + 1))
LOWERCASE = "".join(chr(c) for c in range(ord('a'), ord('z') + 1))

# Return a random integer between min_value and max_value, inclusive.
def randrange(min_value, max_value):
  return random.randrange(min_value, max_value + 1)

# Return a subset with k elements from the specified population,
# where order does not matter.
def unordered_subset(population, k):
  return sorted(random.sample(population, k))

# Return a subset with k elements from the specified population,
# where order does matter.
def ordered_subset(population, k):
  return random.sample(population, k)

# Return a multiset with k elements from the specified population,
# where order does matter.
def ordered_multiset(population, k):
  return [random.choice(population) for _ in range(k)]

# Return a random tree with n nodes, using parent link representation.
# The tree generation is done using Prufer's sequence.
def generate_tree(n):
  # The Prufer's sequence is invalid for tree with only one node,
  # in this case return the only possible tree.
  if n == 1: return []

  # Generate a random Prufer's sequence for a tree with n nodes.
  prufer = [randrange(0, n - 1) for _ in range(n - 2)]
  degree = [prufer.count(i) + 1 for i in range(n)]

  # Initialize a sorted queue to hold nodes with degree one, and list
  # with edges for the constructed tree.
  edges = []
  fringe = [i for i, x in enumerate(degree) if x == 1]
  heapq.heapify(fringe)

  # Process all elements in the Prufer's sequence.
  for x in prufer:
    # Get the next unmatched leaf and push the new edge.
    node = heapq.heappop(fringe)
    edges.append((x, node))

    # Update nodes degrees and push into the fringe those that
    # became leafs (i.e.: have degree 1).
    degree[x] -= 1
    degree[node] -= 1
    if degree[x] == 1:
      heapq.heappush(fringe, x)

  # Get the pair or remaining nodes, add the last edge and return.
  a = heapq.heappop(fringe)
  b = heapq.heappop(fringe)
  edges.append((a, b))
  return edges
