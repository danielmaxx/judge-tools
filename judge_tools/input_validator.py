# -*- coding: utf-8 -*-

import re
import sys

# Class used for validate some input file.
class _FormatValidator:
  # Open the specified file and reset line counter.
  def __init__(self, filename):
    self.line_number = 0
    self.file = open(filename, "rt")

  # Close the opened file on destruction.
  def __del__(self):
    self.file.close()

  # Read a line from the file and validate that it follows some regular expression.
  def readline(self, regexp, *, desc = None):
    # Read a line and increase line counter.
    line = self.file.readline()
    self.line_number += 1

    # Format an error message and validate the regexp, the return the line read.
    error_msg = "Line {line} does not match regexp '{regexp}'.".format(line = repr(line), regexp = regexp) if not desc \
      else "Line {line} does not match regexp '{regexp}' for: {desc}.".format(line = repr(line), regexp = regexp, desc = desc)
    self.check(re.match(regexp, line), error_msg)
    return line

  # Check some condition, if the condition does not hold,
  # print an error message and exit.
  def check(self, valid_flag, error_msg):
    if not valid_flag:
      print("Error at line #{line}: {error}.".format(line = self.line_number, error = error_msg), file = sys.stderr)
      sys.exit(1)

# Interface function used to create a format validator.
def create_validator(filename):
  return _FormatValidator(filename)
