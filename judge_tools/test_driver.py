# -*- coding: utf-8 -*-
import os
import shutil
import sys
import subprocess
import time

# Execute a sequence of commands or a single command.
def execute_script(script, *, stdin = None, stdout = None, **kwargs):
  # Enclose a single line in a tuple, then call each instruction in it.
  if isinstance(script, str): 
    script = (script,)
  for command in script:
    # Show the command and execute it.
    compiled_command = [token.format(**kwargs) for token in command]
    print("    > {command}".format(command = ' '.join(compiled_command)))
    subprocess.check_call(compiled_command, stdin = stdin, stdout = stdout)

class Language:
  # Construtor used to initialize a language.
  def __init__(self, *, name, extension, source_name, executable_name = "",
               compile_script = None, execute_script):
    self.name = name
    self.extension = extension
    self.source_name = source_name
    self.executable_name = executable_name
    self.compile_script = compile_script
    self.execute_script = execute_script

class Solution:
  # Constructor used to initialize a solution.
  def __init__(self, *, name, language, redirect_stdin = False, redirect_stdout = False):
    self.name = name
    self.language = language
    self.redirect_stdin = redirect_stdin
    self.redirect_stdout = redirect_stdout

  # Run this solution using the specified input and output files for the specified problem.
  def compile_and_run(self, problem, input_name, output_name):
    # Create the variables dictionary, including the formatted source name and executable name.
    variables = {"problem": problem.name, "solution": self.name, "language": self.language.name,
                 "extension": self.language.extension, "input": input_name, "output": output_name,}
    print( variables )
    source = variables["solution"] + "." + variables["extension"] #self.language.source_name.format(**variables)
    variables.update([("source", source)])
    executable = variables["solution"] #self.language.executable_name.format(**variables)
    variables.update([("executable", executable)])

    # Execute the compile command to compile this solution.
    if self.language.compile_script is not None:
      print("  Compiling solution '{solution}' in {language} for problem '{problem}'...".format(**variables))
      execute_script(self.language.compile_script, **variables)

    # Open the input and output files to redirect standard streams if necessary.
    input = open(input_name, "rt") if self.redirect_stdin else None
    output = open(output_name, "wt") if self.redirect_stdout else None

    # Use the generated command to run the solution, then print the execution time.      
    print("  Executing solution '{solution}' in {language} for problem '{problem}'...".format(
      problem = problem.name, solution = self.name, language = self.language.name))
    start_time = time.clock()
    execute_script(self.language.execute_script, stdin = input, stdout = output, **variables)
    exec_time = round(time.clock() - start_time, 2)
    print("  Solution '{solution}' in {language} for problem '{problem}' ran in {time} seconds.".format(
      time = exec_time, **variables))

    # Remove the generated executable if there is one.
    time.sleep(0.5)
    if executable != "":
      os.remove(executable)

class Problem:
  # Constructor used to initialize a problem.
  def __init__(self, *, name, directory = "", generator = None, validator = None, valid_solution, test_solutions = ()):
    self.name = name
    self.generator = generator
    self.validator = validator
    self.valid_solution = valid_solution
    self.test_solutions = test_solutions
    self.directory = directory

  # Generate the input file for the problem, using the generator
  # command specified during initialization.
  def generate_input(self):
    # Check if there is a generator script, then execute it.
    if self.generator is not None:
      print("  Generating input file for problem '{problem}'...".format(problem = self.name))
      execute_script(self.generator, problem = self.name)
      print("  Input file for problem '{problem}' successfully generated.".format(problem = self.name))

  # Validate the generated input file for the problem, using the
  # validator command specified during initialization.
  def validate_input(self):
    # Check if there is a validator script, then execute it.
    if self.validator is not None:
      # Execute the constructed command to validate the input file.
      print("  Validating input file for problem '{problem}'...".format(problem = self.name))
      execute_script(self.validator, problem = self.name)
      print("  Input file for problem '{problem}' successfully validated.".format(problem = self.name))
    else:
      print("  WARNING: No input validator found for problem '{problem}'".format(problem = self.name))

  # Generate the correct output for the generated input file,
  # running the valid solution specified during initialization.
  def generate_output(self):
    # Generate input and output file names and use them to compile
    # and run the valid solution.
    print("  Generating output file for problem '{problem}'...".format(problem = self.name))
    input_name = "{problem}.in".format(problem = self.name)
    output_name = "{problem}.out".format(problem = self.name)
    self.valid_solution.compile_and_run(self, input_name, output_name)
    print("  Output file for problem '{problem}' successfully generated.".format(problem = self.name))

  # Validate other output using the valid output as reference.
  def validate_output(self, solution, valid_output, other_output):
    # Initialize error counter and read lines from the output files.
    print("  Validating {other_output} output with correct results in {valid_output}...".format(
      other_output = other_output, valid_output = valid_output))
    error_count = 0
    valid_lines = open(valid_output, "rt").readlines()
    other_lines = open(other_output, "rt").readlines()

    # Check each line in the files and look for errors.
    for line_number, (valid_line, other_line) in enumerate(zip(valid_lines, other_lines), 1):
      if valid_line != other_line:
        if error_count == 0:
          print("Diffrences found between '{valid}' and '{other}'.".format(
            valid = valid_output, other = other_output), file = sys.stderr)
        # Print the lines with the error and increase the counter.
        print("{line}:< {text}".format(line = line_number, text = repr(valid_line)), file = sys.stderr)
        print("{line}:> {text}".format(line = line_number, text = repr(other_line)), file = sys.stderr)
        error_count += 1

    # Check if there are extra lines or missing at the checked output.
    if len(other_lines) > len(valid_lines):
      print("{extra} extra line(s) found at {file}.".format(
        extra = len(other_lines) - len(valid_lines), file = other_output), file = sys.stderr)
    if len(other_lines) < len(valid_lines):
      print("{missing} missing line(s) in {file}.".format(
        missing = len(valid_lines) - len(other_lines), file = other_output), file = sys.stderr)

    # Adjust error count by line differences and return it.
    error_count += abs(len(other_lines) - len(valid_lines))
    return error_count

  def test_solution(self, solution):
    # Generate input and output file names for the solution to test.
    print("  Testing solution '{solution}' in {language} for problem '{problem}'...".format(
      problem = self.name, solution = solution.name, language = solution.language.name))
    input_name = "{problem}.in".format(problem = self.name)
    valid_output_name = "{problem}.out".format(problem = self.name)
    output_name = "{problem}-{solution}-{language}.out".format(
      problem = self.name, solution = solution.name, language = solution.language.name)

    # Compile and run solution using the input and output names, then
    # check if the output is valid.
    solution.compile_and_run(self, input_name, output_name)
    error_count = self.validate_output(solution, valid_output_name, output_name)
    if error_count == 0:
      # Remove the output file and print success message.
      os.remove(output_name)
      print("  Solution '{solution}' in {language} for problem '{problem}' successfully tested.".format(
        problem = self.name, solution = solution.name, language = solution.language.name))
    else:
      # Print error message and leave the output file for manual checking.
      print("  Solution '{solution}' in {language} for problem '{problem}' had {errors} error(s).".format(
        problem = self.name, solution = solution.name, language = solution.language.name, errors = error_count))

  def test(self):
    # Move the current directory to the problem directory.
    print("Testing problem '{problem}'...".format(problem = self.name))
    os.chdir("problems/{directory}".format(directory = self.directory))

    # Generate the input, validate it and then run the valid
    # solution to generate the output.
    self.generate_input()
    self.validate_input()
    self.generate_output()

    # Run all test solutions and validate them, and print a warning message
    # if there are no test solutions.
    for solution in self.test_solutions:
      self.test_solution(solution)
    if not self.test_solutions:
      print("  WARNING: No test solutions found for problem '{problem}'".format(problem = self.name))

    # Go back to the starting directory and move data files to the data directory.
    os.chdir("../../")
#    print( os.getcwd() )
    shutil.copy("problems/{directory}/{problem}.in".format(directory = self.directory, problem = self.name), "data/")
    shutil.copy("problems/{directory}/{problem}.out".format(directory = self.directory, problem = self.name), "data/")
    print("Problem '{problem}' successfully tested.".format(problem = self.name))
    print()

def test_problems(problems):
  # Get problems to test from command line, if there
  # are no problems then test them all.
  target_problems = [name for name in sys.argv[1:] if name in problems]
  if not target_problems:
    target_problems = sorted(problems.keys())

  # Create the data directory and ignore any error (like directory already exists).
  try: os.mkdir("data/")
  except OSError: pass

  # Test all problems in the array with targets.
  for name in target_problems:
    problems[name].test()
