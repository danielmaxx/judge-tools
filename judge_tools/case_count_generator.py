# -*- coding: utf-8 -*-

# Class used to store a file generator to append to the output file.
class _TestCaseFile:
  # Initialize the generator with the name of the file to append.
  def __init__(self,*, input_file):
    self.input_file = input_file

  # Return the number of test cases inside the file.
  def case_count(self):
    try:
      # Open the file and use the first line as test case count.
      input = open(self.input_file, "rt")
      return int(input.readline())
    finally:
      input.close()

  # Run the generator and write all test cases to the specified output file.
  def run(self, output):
    try:
      # Append all lines except the first one to the output file.
      input = open(self.input_file, "rt")
      for line in input.readlines()[1:]:
        output.write(line)
    finally:
      input.close()

# Class used to store a function to generate test cases.
class _TestCaseFunction:
  # Initialize the generator with the specified function and count.
  def __init__(self, *, function, count):
    self.function = function
    self.count = count

  # Return the number of test cases to generate.
  def case_count(self):
    return self.count

  # Run the generator and write all test cases to the specified output file.
  def run(self, output):
    # Call the generator function as many times at it was specified, using the
    # specified output file and giving the index to each call.
    for index in range(self.count):
      self.function(index, output)

# List of generators to run when the generate_cases function is called.
_generators = []

# Add a file to the list of case files to append.
def add_file(*, file):
  _generators.append(_TestCaseFile(input_file = file))

# Add a function generator to the list of generators.
def add_function(*, f, n):
  _generators.append(_TestCaseFunction(function = f, count = n))

# Generate all cases and write them to the specified output file.
def generate_cases(*, output):
  # Write the case count at the begining of the file, the run all generators
  # to the specified output file.
  count = sum(generator.case_count() for generator in _generators)
  output.write("{count}\n".format(count = count))
  for generator in _generators: generator.run(output)

