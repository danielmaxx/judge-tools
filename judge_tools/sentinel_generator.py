# -*- coding: utf-8 -*-

# Class used to store a file generator to append to the output file.
class _TestCaseFile:
  # Initialize the generator with the name of the file to append.
  def __init__(self,*, input_file):
    self.input_file = input_file

  # Run the generator and write all test cases to the specified output file.
  def run(self, output, sentinel):
    try:
      # Append all lines to the output file, except the last one if
      # there is a sentinel.
      input = open(self.input_file, "rt")
      lines = input.readlines()
      if sentinel: lines.pop()
      for line in lines: output.write(line)
    finally:
      input.close()

# Class used to store a function to generate test cases.
class _TestCaseFunction:
  # Initialize the generator with the specified function and count.
  def __init__(self, *, function, count):
    self.function = function
    self.count = count

  # Return the number of test cases to generate.
  def case_count(self):
    return self.count

  # Run the generator and write all test cases to the specified output file.
  def run(self, output, _):
    # Call the generator function as many times at it was specified, using the
    # specified output file and giving the index to each call.
    for index in range(self.count):
      self.function(index, output)

# List of generators to run when the generate_cases function is called.
_generators = []

# Add a file to the list of case files to append.
def add_file(*, file):
  _generators.append(_TestCaseFile(input_file = file))

# Add a function generator to the list of generators.
def add_function(*, f, n):
  _generators.append(_TestCaseFunction(function = f, count = n))

# Generate all cases and write them to the specified output file.
def generate_cases(*, output, sentinel = None):
  # Run all generators and the append the sentinel at the end.
  # Use EOF if no sentinel was specified.
  for generator in _generators:
    generator.run(output, sentinel)
  if sentinel: output.write(sentinel)

