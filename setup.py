# -*- coding: utf-8 -*-

from distutils.core import setup

setup(name='judge_tools',
      version='1.0',
      author='Jorge Bernadas',
      author_email='jbernadas@gmail.com',
      download_url='https://bitbucket.org/danielmaxx/judge-tools.git',
      description='A set of judge utilities',
#      package_dir={'':'src'},
      packages=['judge_tools'],
      classifiers=['Development Status :: Beta',
                   'Intended Audience :: Developers',
                   'Natural Language :: English',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 3.0+',
                   'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
                   'License :: OSI Approved :: GNU Affero General Public License v3',
                   'Topic :: Internet',
                   'Topic :: Scientific, Programming Contests',
                  ],
     )
